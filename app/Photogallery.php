<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
 use DB;

class Photogallery extends Model
{
    protected  $table = 'photos';

 	public static function getPhoto() {
        $photogallery = DB::table('photos')
        /// ->join('albums', 'photos.id', '=', 'photos.albumid')
        ->groupBy('photos.albumid')
        ->orderBy('id', 'desc')
        ->where('status',1)
        ->get();
        return $photogallery;
    }
    public static function getphotoManage(){
          $photomanage = DB::table('photos')
        ->orderBy('id', 'asc')
        ->get();
        return $photomanage;
    }

    public static function photdetail($id) {
        $album = DB::table('photos')
               /// ->join('tbl_photo_album', 'tbl_photo.alb_id', '=','tbl_photo_album.id')
                ->where('albumid', '=',$id)
                 ->orderBy('id', 'desc')
                ->get();
        return $album;
    }

    
    //   public function getPhoto()
    // {
      
    //     return $this->hasMany('App\photos');
    // }
     
}
