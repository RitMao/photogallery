<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class userModel extends Model
{
     protected  $table = 'users';

     public static function getUser(){
     	$user=DB::table('users')
     	->get();
     	return $user;
     }
}
