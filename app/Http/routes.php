<?php /*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('index');
});

Route::group(['prefix' => 'gallery'], function () {

			Route::get('home','PhotosController@getHome');
            Route::get('/','PhotosController@getGallery');
            // Route::get('detail','PhotosController@detailGallery');
           Route::get('detail/{id}', ['as' => 'detail', 'uses' => 'PhotosController@detailGallery']);
    });
Route::get('administrator','AdminphotoController@logIn');
Route::post('signin','AdminphotoController@postLogin');
Route::get('signup','AdminphotoController@signUp');
Route::post('save','AdminphotoController@postSignup');

Route::get('delete/{id}', ['as' => 'delte', 'uses' => 'AdminphotoController@deletephoto']);
 Route::get('editphoto/{id}',['as' => 'editphoto', 'uses' => 'AdminphotoController@geteditphoto']);
  Route::post('updatuser/{id}', ['as' => 'updatuser', 'uses' => 'userController@updateUser']);          
Route::group(['prefix' => 'page'], function () {

			Route::get('/','AdminphotoController@getIndex');
			Route::get('photo','AdminphotoController@photoManagemen');
			Route::get('addphoto','AdminphotoController@createPhoto');
            Route::post('savephoto','AdminphotoController@postPhoto');
            Route::get('phototatus/{id}/{status}', ['as' => 'phototatus', 'uses' => 'AdminphotoController@photoStatus']);
           ///Album Management
            Route::get('abums','albumController@index');
            Route::get('create','albumController@create');
		   Route::post('save','albumController@postSave');
           Route::get('delete/{id}', ['as' => 'delete', 'uses' => 'albumController@getdeletealbum']);
           Route::get('edit/{id}',['as' => 'edit', 'uses' => 'albumController@getdeletealbum']);
           Route::get('statusalbum/{id}/{status}', ['as' => 'statusalbum', 'uses' => 'albumController@albumStatus']);

         ///User management
         Route::get('user','userController@index'); 
         Route::get('createuser','userController@createUser'); 
         Route::post('saveuser','userController@postUser');
         Route::get('userdelete/{id}', ['as' => 'delteuser', 'uses' => 'userController@deleteUser']); 
        Route::get('edituser/{id}',['as' => 'edituser', 'uses' => 'userController@getedituser']);
        Route::get('userstatus/{id}/{status}', ['as' => 'userstatus', 'uses' => 'userController@userStatus']);

       
    });