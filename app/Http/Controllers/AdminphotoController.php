<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\photoRequest;
use App\Http\Controllers\Controller;
use View;
use form;
use Input;
use Validator;
use Redirect;
use Hash;
use App\User;
use Session;
use flash;
use Auth;
use App\Photogallery;
use Html;
use App\Albums;
use DB;
use URL;
use File;
class AdminphotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
protected $data_photo = [
        'ID' => '',
        'albumid' => '',
        'photoname' => '',
        'image' => '',
        'desc' => '',
    ];

public function logIn(){
        $data['URL'] = 'signup';
        return View::make('admin.login');
    }



public function postLogin(){
    $data = array(
        'email' => Input::get('email'),
        'password' => Input::get('password')
    );
   $rules = [];
    $validate = Validator::make($data,$rules);
    if($validate->fails()){
      return Redirect::to('administrator')->withErrors($validate)->withInput();
    } else{
        $data['password'] = Hash::make($data['password']);
        $data['email']=Hash::make($data['email']);
        $attempt = Auth::attempt(array('email' => Input::get('email'), 'password' => Input::get('password'),'status'=>1));
        if ($attempt) {
            return View::make('admin.index');
        }else{
        Session::flash('message', 'Your user name or password incorrect');
        Session::flash('alert-class', 'alert-danger');
             return View::make('admin.login');
                 
        }
    }

}

public function signUp(){
     $data['URL'] = 'signup';
        return View::make('admin.register',$data);
}

public function postSignup() {
        $user = [
            'firsname' => Input::get('firsname'),
            'lastname' => Input::get('lastname'),
            'email' => Input::get('email'),
            'password' => Input::get('password'),
        ];
        $rules = [
            'firsname' => 'required',
            'lastname' => 'required',
            'email' => 'required',
            'password' => 'required',
                // not 
        ];
        $valid = Validator::make($user, $rules);
            if ($valid->passes()) {
            $password = Input::get('password');
            $confirmpassword=Input::get('cornfirmpassword');
            if($password==$confirmpassword){
            $password = Hash::make($password);
            }else{
                Session::flash('message', 'Your password is not match!');
                Session::flash('alert-class', 'alert-danger');
                return View::make('admin.register');
            }         
            $user = new User;
            $user->firsname = Input::get('firsname');
            $user->lastname = Input::get('lastname');
            $user->email = Input::get('email');
            $user->password = $password;
            $user->status = 1;
            $user->save();
            ///return 'Successfully';
           return View::make('admin.login');
        } else {
            return Redirect::back()
                ->withErrors($valid)
                ->withInput();
        }         
    }
public function getIndex()
    {
     return View::make('admin.index');
    }
public function photoManagemen(){
        $data['Records'] = Photogallery::getphotoManage();
        return View::make('admin.photo',$data);
    }
public function createPhoto()
    {
        $data['PageTitle'] = 'add photo';
        $data['URL'] = "page/savephoto";
        $data['album'] = Albums::getAlbums();
        $data['photo'] = (object) $this->data_photo;
        return view('admin.create', $data);
    }
public function postPhoto(photoRequest $request) {
        try {
            DB::beginTransaction();
            $data = $request->all();
            $id = Input::get("ID");
            $ObjTmp = "";
            if (!empty($id)) {
               $arr_data = Photogallery::find($id)->toArray();
                         
                $arr_data['albumid'] = Input::get('albumid');
                $arr_data['photoname'] = Input::get('photoname');
                $arr_data['desc'] = Input::get('desc');
                DB::table('photos')->where('ID', $id)->update($arr_data);                        
            }else { 
                $destinationPath = 'img/photos'; // upload path
                $extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(11111, 99999) . '.' . $extension; // renameing image
                Input::file('image')->move($destinationPath, $fileName); // uploading file to given path
                $arr_data = new Photogallery;               
                $arr_data['albumid'] = Input::get('albumid');
                $arr_data['photoname'] = Input::get('photoname');
                $arr_data['desc'] = Input::get('desc');
                $arr_data->image = $fileName;
                $arr_data->save();               
            }
            DB::commit();
            return Redirect::to(URL::to('page/photo'))->with('msg_success', trans('glouble.Update success'));
        } catch (Exception $e) {
            DB::rollback();
            return Redirect::to(URL::to('page/create'))->with('msg_error', $e->geotMessage());
        }
    }
public function geteditphoto($id = ""){
        $data['PageTitle'] ='Edit photo';
        $data['URL'] = "page/savephoto";
        if (!empty($id)) {
            $data['album'] = Albums::getAlbums();
            $data['photo'] = Photogallery::find($id);
        } else {
            return Redirect::to(URL::to('page/photo'))
            ->with('msg_error','Record not found.');
        }
       return view('admin.create',$data);
    }
public function photoStatus($status, $id) {
        ($status == 0) ? $status = 1 : $status = 0;
        $user = Albums::find($id);
        $update = DB::table('photos')
                ->where('ID', $id)
                ->update(array('status' => $status));
        Session::flash('message', 'Successfully change status!');
        return Redirect::back();
    }
public function deletephoto($id) {
        $photo = Photogallery::find($id);
        DB::table('photos')->where('ID', $id)->delete();
        DB::commit();
        $file = 'img/photo/' . $photo->files;
        File::delete($file);
        return Redirect::to(URL::to('page/photo'))
                        ->with('success', 'photo is deleted!');
    }
}
