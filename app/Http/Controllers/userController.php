<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use form;
use Input;
use Validator;
use Redirect;
use Hash;
use App\User;
use Session;
use flash;
use Auth;
use App\Photogallery;
use Html;
use App\Albums;
use DB;
use URL;
use File;
use App\userModel;
class userController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public $user_data = [
        'id' => '',
        'firsname' => '',
        'lastname' => '',
        'email' => '',
        'password' =>'',
       
    ];

    public function index()
    {
        $data['user']= userModel::getUser();
        return View::make('admin.user',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
public function createUser(){
        $data['user'] = (object) $this->user_data;
        return View::make('admin.createuser',$data);
        }

public function postUser() {
        $user = [
            'firstname' => Input::get('firstname'),
            'lastname' => Input::get('lastname'),
            'email' => Input::get('email'),
            'password' => Input::get('password'),
        ];
        $rules = [
            'firstname' => 'required',
            'lastname' => 'required',
            'email' => 'required',
            'password' => 'required',         
        ];
        $valid = Validator::make($user,$rules);
            if ($valid->passes()) {              
            $password = Input::get('password');
            $confirmpassword=Input::get('cornfirmpassword');
            if($password==$confirmpassword){
            $password = Hash::make($password);
            }else{     
                Session::flash('message', 'Your password is not match!');
                Session::flash('alert-class', 'alert-danger');
                return View::make('admin.createuser');
            }         
            $user = new userModel;
            $user->firsname = Input::get('firstname');
            $user->lastname = Input::get('lastname');
            $user->email = Input::get('email');
            $user->password = $password;
            $user->status = 1;
            $user->save();
          return Redirect::to(URL::to('page/user'));
        } else {
            die('Fail');
            return Redirect::back()
                ->withErrors($valid)
                ->withInput();
        }         
    }
     public function getedituser($id = "") {
        if (!empty($id)) {
            $data['user'] = userModel::find($id);
        } else {
            return Redirect::to(URL::to('page/user'))
            ->with('msg_error', 'Record not found.');
        }
        return view('admin.edituser',$data);
    }
     public function updateUser($id) {
        $data = [
        ];
        $rules = [
        ];
        $valid = Validator::make($data, $rules);
        if ($valid->passes()) {
            $data = userModel::find($id);
            $data->firsname = Input::get('firstname');
            $data->lastname = Input::get('lastname');
             $data->email = Input::get('email');
            $data->save();
            return Redirect::to(URL::to('page/user'))
                            ->with('success', 'menu is success saved!');
        } else {
            return Redirect::back()
                            ->withErrors($valid)
                            ->withInput();
        }
    }
  public function userStatus($status, $id) {
        ($status == 0) ? $status = 1 : $status = 0;
        $user = userModel::find($id);
        $update = DB::table('users')
                ->where('id', $id)
                ->update(array('status' => $status));
        Session::flash('message', 'Successfully change status!');
        return Redirect::back();
    }
  public function deleteUser($id) {
        $photo = userModel::find($id);
        DB::table('user')->where('id', $id)->delete();
        DB::commit();
        return Redirect::to(URL::to('page/user'))
                        ->with('success', 'photo is deleted!');
    }
}
