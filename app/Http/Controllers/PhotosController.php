<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use View;
use App\Photogallery;
use form;

class PhotosController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function getHome()
    {
       return View::make('index');
    }

    public function getGallery()
    { 

    // $data['Records'] = \App\Photogallery::getPhoto();
     $data['Records'] = Photogallery::getPhoto();
    return View::make('site.gallery',$data);

    }

    public function detailGallery($id){
        $data['detail']=Photogallery::photdetail($id);
        return View::make('site.detailgallery',$data);
    }

}
