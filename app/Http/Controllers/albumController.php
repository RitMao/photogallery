<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\albumRequest;
use App\Http\Controllers\Controller;
use App\Albums;
use View;
use DB;
use Input;
use Redirect;
use file;
use URL;
use Html;
use Session;
class albumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $album_data = [
        'ID' => '',
        'suerID' => '',
        'albumdesc' => '',
        'albumname' => '',
       
    ];

    public function index()
    {
        $data['record'] = Albums::getAlbums();
        return View::make('admin.album',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['PageTitle'] = 'Add album';
        $data['URL'] = "page/save";
        $data['albums'] = (object) $this->album_data;
        return View::make('admin.creatalbum',$data);
    }
     public function postSave(albumRequest $request) {
        try {
            DB::beginTransaction();
            $data = $request->all();
            $id = Input::get("ID");
            $ObjTmp = "";
            if (!empty($id)) {
                $arr_data = Albums::find($id)->toArray();
                $arr_data['albumname'] = Input::get('albumname');
                DB::table('albums')->where('ID', $id)->update($arr_data);
            } else {
                $arr_data = new Albums;
                $arr_data['suerID'] = Input::get('suerID');
                $arr_data['albumname'] = Input::get('albumname'); 
                $arr_data->save(); 
            }
            DB::commit();
             return Redirect::to(URL::to('page/abums'));
        } catch (Exception $e) {
            DB::rollback();
            return Redirect::to(URL::to('page/create'))->with('msg_error', $e->geotMessage());
        }
    }
    public function geteditAlbums($id = "") {
        $data['PageTitle'] ='Edit albums';
        $data['URL'] = "page/save";
        if (!empty($id)) {
            $data['albums'] = Albums::find($id);
        } else {
            return Redirect::to(URL::to('page/abums'))
            ->with('msg_error', 'Record not found.');
        }
        return view('admin.creatalbum',$data);
    }
public function albumStatus($status, $id) {
        ($status == 0) ? $status = 1 : $status = 0;
        $user = Albums::find($id);
        $update = DB::table('albums')
                ->where('ID', $id)
                ->update(array('status' => $status));
        Session::flash('message', 'Successfully change status!');
        return Redirect::back();
    }
    public function getdeletealbum($id) {
        $photo = Albums::find($id);
        DB::table('albums')->where('ID', $id)->delete();
        DB::commit();
        return Redirect::to(URL::to('page/abums'))
                        ->with('success', 'photo is deleted!');
    }

}
