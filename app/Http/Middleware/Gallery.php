<?php

namespace App\Http\Middleware;

use Closure;

class Gallery
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    protected $table = 'photos';
    public function handle($request, Closure $next)
    {
        return $next($request);
    }

    public function getPhotos(){
         $photos = DB::table('photos')
                ->get();
        return $photos;
    }


}
