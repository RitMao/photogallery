<?php

namespace App;
use DB;

use Illuminate\Database\Eloquent\Model;

class Albums extends Model
{
    protected  $table = 'albums';

    public static function getAlbums(){
    	$albums= DB:: table('albums')
    	->get();
    	return $albums;
    
}
}
