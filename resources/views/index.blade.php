@extends('layouts.site.master')
        @section('sidebar')
        @parent
        <ul class="slides">
                <li>
                    <img src="{{asset('img/slides/slide1.jpg')}}" alt=""/>
                    <div class="text1">A just world</div>
                    <div class="text2">Free from poverty</div>
                </li>
                <li>
                    <img src="{{asset('img/slides/slide2.jpg')}}" alt=""/>
                </li>
                <li>
                    <img src="{{asset('img/slides/slide3.jpg')}}" alt=""/>
                </li>
                <li>
                    <img src="{{asset('img/slides/slide4.jpg')}}" alt=""/>
                </li>
                <li>
                    <img src="{{asset('img/slides/slide5.jpg')}}" alt=""/>
                </li>
            </ul>
        @endsection
    <!-- Info -->
    @section('content')
    <section class="info">
        <div class="center cf">
            <div class="col-4 ">
                <div class="icon i1"></div>
                <div class="text">
                    <h3>Advanced technology</h3>
                    <input type="submit" name="save" class="test">
                    <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, printer took a galley. of type and </p>
                </div>
            </div>
            <div class="col-4">
                <div class="icon i2"></div>
                <div class="text">
                    <h3>Healthcare solutions</h3>
                    <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, printer took a galley. of type and </p>
                </div>
            </div>
            <div class="col-4">
                <div class="icon i3"></div>
                <div class="text">
                    <h3>24/7 Availability</h3>
                    <p>Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, printer took a galley. of type and </p>
                </div>

            </div>
        </div>
    </sectin>

    <!-- Donate -->
    <section class="donate">

        <div class="cf">
            <div class="col-6 text">
                <h3>Thank you!</h3>
                <h4>You’ve helped us raise a staggering</h4>
                <h5><span>£53,370,743</span> so far!</h5>
            </div>
            <div class="col-6 but">
                <a href="donate.html">Donate/pay in your money</a>
            </div>
        </div>

        <!-- -->
        <div id="line">
            <div class="way">
                <div class="bar"></div>
            </div>
        </div>

        <!-- -->
        <div class="share">
            <span>Share: </span>
            <a class="twitter" href=""></a>
            <a class="facebook" href=""></a>
            <a class="pinterest" href=""></a>
            <a class="skype" href=""></a>
        </div>

    </section>

    <!-- Latest Donations -->
    <section class="latest-donations">

        <div class="center cf">
            <h2>Latest Donations</h2>

            <div class="peoples">
                <div class="col-2i">
                    <div class="circle">
                        <img src="http://placehold.it/94x94" alt=""/>
                    </div>
                    <div class="name">Jack Daniels</div>
                    <div class="state">Manager</div>
                    <div class="donated">
                        <div class="left">Donated</div>
                        <div class="right">$3,200</div>
                    </div>
                </div>
                <div class="col-2i">
                    <div class="circle">
                        <img src="http://placehold.it/94x94" alt=""/>
                    </div>
                    <div class="name">Mark Ougust</div>
                    <div class="state">Entrepreneur</div>
                    <div class="donated">
                        <div class="left">Donated</div>
                        <div class="right">$3,200</div>
                    </div>
                </div>
                <div class="col-2i">
                    <div class="circle">
                        <img src="http://placehold.it/94x94" alt=""/>
                    </div>
                    <div class="name">Jack Daniels</div>
                    <div class="state">Director of the Bank</div>
                    <div class="donated">
                        <div class="left">Donated</div>
                        <div class="right">$3,200</div>
                    </div>
                </div>
                <div class="col-2i">
                    <div class="circle">
                        <img src="http://placehold.it/94x94" alt=""/>
                    </div>
                    <div class="name">Marey Weeli</div>
                    <div class="state">Historian</div>
                    <div class="donated">
                        <div class="left">Donated</div>
                        <div class="right">$3,200</div>
                    </div>
                </div>
                <div class="col-2i">
                    <div class="circle">
                        <img src="http://placehold.it/94x94" alt=""/>
                    </div>
                    <div class="name">Jack Daniels</div>
                    <div class="state">Entrepreneur</div>
                    <div class="donated">
                        <div class="left">Donated</div>
                        <div class="right">$3,200</div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <!-- How can you help -->
    <section class="how-help">
        <div class="top-border"></div>

        <div class="center cf">
            <h2>How Can You Help?</h2>

            <div class="col-6 left">
                <h3>Help for cause</h3>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
                    when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                    It has survived not only five centuries, but also the leap into electronic typesetting.
                </p>
                <div class="col-6 c1">Media</div>
                <div class="col-6 c2">Projection</div>
                <div class="col-6 line"><div class="li"></div></div>
                <div class="col-6 line"><div class="li"></div></div>
                <div class="col-6 c3">Mobilization</div>
                <div class="col-6 c4">Support</div>
                <a class="more" href="about.html">Read More</a>
            </div>
            <div class="col-6 right">
                <iframe src="//www.youtube.com/embed/uF47o0wOncA" allowfullscreen></iframe>
            </div>
        </div>
    </section>

    <!-- How can you help -->
    <section class="news">
        <div class="center cf">

            <div class="col-3 news">
                <h3>News</h3>

                <section>
                    <div class="time">5:00am-5:30am</div>
                    <a href="single.html">There are many variations</a>
                    <p>
                        Lorem Ipsum is simply dummy text
                        of the printing and...
                    </p>

                    <div class="info cf">
                        <div class="reply">36</div>
                        <div class="comment">15</div>
                        <div class="tag">Conferences</div>
                    </div>
                </section>

                <section>
                    <div class="time">5:00am-5:30am</div>
                    <a href="single.html">There are many variations</a>
                    <p>
                        Lorem Ipsum is simply dummy text
                        of the printing and...
                    </p>

                    <div class="info cf">
                        <div class="reply">36</div>
                        <div class="comment">15</div>
                        <div class="tag">Conferences</div>
                    </div>
                </section>

                <a class="more" href="blog.html">More news</a>

            </div>
            <div class="col-6 about">
                <h3>About us</h3>

                <img src="http://placehold.it/529x197" alt=""/>
                <p>
                    Lorem Ipsum is simply dummy text of the printing and...typesetting industry.
                    Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                    galley of type and scrambled it to make a type specimen book.
                    It has survived not only five centuries, but also the leap into electronic typesetting,
                    remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets
                    containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                </p>

            </div>
            <div class="col-3 photos">
                <h3>
                    Photos
                    <span class="control">
                        <span class="left"></span>
                        <span class="right"></span>
                    </span>
                </h3>

                <div id="photo-slider">
                    <ul class="slides">
                        <li>
                            
                            <div class="col-6">
                                <a href="#">
                                    <img src="http://placehold.it/128x113" alt=""/>
                                    <span class="mask"></span>
                                </a>
                            </div>
                            <div class="col-6">
                                <a href="#">
                                    <img src="http://placehold.it/128x113" alt=""/>
                                    <span class="mask"></span>
                                </a>
                            </div>
                            <div class="col-6">
                                <a href="#">
                                    <img src="http://placehold.it/128x113" alt=""/>
                                    <span class="mask"></span>
                                </a>
                            </div>
                        </li>

                        <li>
                            <div class="col-6">
                                <a href="#">
                                    <img src="http://placehold.it/128x113" alt=""/>
                                    <span class="mask"></span>
                                </a>
                            </div>
                            <div class="col-6">
                                <a href="#">
                                    <img src="http://placehold.it/128x113" alt=""/>
                                    <span class="mask"></span>
                                </a>
                            </div>
                            <div class="col-6">
                                <a href="#">
                                    <img src="http://placehold.it/128x113" alt=""/>
                                    <span class="mask"></span>
                                </a>
                            </div>
                            <div class="col-6">
                                <a href="#">
                                    <img src="http://placehold.it/128x113" alt=""/>
                                    <span class="mask"></span>
                                </a>
                            </div>
                            <div class="col-6">
                                <a href="#">
                                    <img src="http://placehold.it/128x113" alt=""/>
                                    <span class="mask"></span>
                                </a>
                            </div>
                            <div class="col-6">
                                <a href="#">
                                    <img src="http://placehold.it/128x113" alt=""/>
                                    <span class="mask"></span>
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
    </section>
    <!-- Section Bottom -->
    <section class="bottom">

        <div class="center cf">
            <div class="col-4">
                <div class="img">
                    <img src="http://placehold.it/327x192" alt=""/>
                    <div class="text">
                        <h3>Animal welfare institute</h3>
                        <a href="single.html">Read more</a>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="img">
                    <img src="http://placehold.it/327x192" alt=""/>
                    <div class="text">
                        <h3>Child protection</h3>
                        <a href="single.html">Read more</a>
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="img">
                    <img src="http://placehold.it/327x192" alt=""/>
                    <div class="text">
                        <h3>Green construction</h3>
                        <a href="single.html">Read more</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Section Bottom -->
    @endsection