@extends('layouts.site.master');
    <!-- Top Page Nav -->
      @section('content')
    <section class="top-page-nav">
        <div class="center cf">

            <h2>Gallery</h2>

            <div class="navi">
                <a href="#">Home</a>
                <span></span>
                <a href="#">Social events </a>
                <span></span>
                <a class="active" href="#">Gallery</a>
            </div>

        </div>
    </section>

    <div class="center cf">

        <!-- Gallery -->
        <section class="col-8 blog cf">
            <div class="gallery">
                <div class="menu cf">
                    <a class="active" href="">All</a>
                    <a href="">Charity</a>
                    <a href="">Nature</a>
                    <a href="">Friendship</a>
                    <a href="">Life</a>
                </div>
  
                <div class="photos cf">
 			 @foreach  ($detail as $gallery)
 			    <div class="col-6">
                        <a href='{{asset("img/photos/$gallery->image")}}' data-lightbox="roadtrip">
                            <img src='{{asset("img/photos/$gallery->image")}}'  alt=""/>
                            <span class="mask"></span>
                            <div class="like"><p>37</p></div>
                        </a>
                    </div>                      
                 @endforeach 
                </div>

            </div>

            <div class="pagination cf">
                <a class="left" href="#"></a>
                <a href="#">1</a>
                <a href="#">2</a>
                <a class="active" href="#">3</a>
                <a href="#">4</a>
                <a href="#">5</a>
                <a class="right" href="#"></a>
            </div>

        </section>

        <!-- Aside Blog -->
        <aside class="col-4 blog cf">

            <form action="#" method="post">
                <input id="search" type="search" placeholder="looking for something?"/>
                <input id="submit" type="submit" value=""/>
            </form>

            <!-- ASIDE DYNAMIC -->
            <div id="aside-dynamic">

                <div class="menu cf">
                    <div class="active" data-attr="m1">Popular</div>
                    <div data-attr="m2">Latest</div>
                    <div data-attr="m3">Recent Comments</div>
                </div>

                <div class="content m1">
                    <div class="item cf">
                        <div class="col-3">
                            <img src="http://placehold.it/77x77" alt=""/>
                        </div>
                        <div class="col-9">
                            <a href="single.html" class="title">Care for Children</a>
                            <p>
                                Mauris ac gravida tellus. Etiam non arcu
                                nisl, eget fermentum mauris.
                            </p>
                            <div class="info">
                                <div class="date">February 12, 2014,</div>
                                <div class="comments">2 comments</div>
                            </div>
                        </div>
                    </div>
                    <div class="item cf">
                        <div class="col-3">
                            <img src="http://placehold.it/77x77" alt=""/>
                        </div>
                        <div class="col-9">
                            <a href="single.html" class="title">Our Children Matter</a>
                            <p>
                                Mauris ac gravida tellus. Etiam non arcu
                                nisl, eget fermentum mauris.
                            </p>
                            <div class="info">
                                <div class="date">February 12, 2014,</div>
                                <div class="comments">2 comments</div>
                            </div>
                        </div>
                    </div>
                    <div class="item cf">
                        <div class="col-3">
                            <img src="http://placehold.it/77x77" alt=""/>
                        </div>
                        <div class="col-9">
                            <a class="title" href="single.html">A Standard Post Format</a>
                            <p>
                                Mauris ac gravida tellus. Etiam non arcu
                                nisl, eget fermentum mauris.
                            </p>
                            <div class="info">
                                <div class="date">February 12, 2014,</div>
                                <div class="comments">2 comments</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="content m2" style="display: none">
                    <div class="item cf">
                        <div class="col-3">
                            <img src="http://placehold.it/77x77" alt=""/>
                        </div>
                        <div class="col-9">
                            <a href="single.html" class="title">Our Children Matter</a>
                            <p>
                                Mauris ac gravida tellus. Etiam non arcu
                                nisl, eget fermentum mauris.
                            </p>
                            <div class="info">
                                <div class="date">February 12, 2014,</div>
                                <div class="comments">2 comments</div>
                            </div>
                        </div>
                    </div>
                    <div class="item cf">
                        <div class="col-3">
                            <img src="http://placehold.it/77x77" alt=""/>
                        </div>
                        <div class="col-9">
                            <a href="single.html" class="title">Care for Children</a>
                            <p>
                                Mauris ac gravida tellus. Etiam non arcu
                                nisl, eget fermentum mauris.
                            </p>
                            <div class="info">
                                <div class="date">February 12, 2014,</div>
                                <div class="comments">2 comments</div>
                            </div>
                        </div>
                    </div>
                    <div class="item cf">
                        <div class="col-3">
                            <img src="http://placehold.it/77x77" alt=""/>
                        </div>
                        <div class="col-9">
                            <a class="title" href="single.html">A Standard Post Format</a>
                            <p>
                                Mauris ac gravida tellus. Etiam non arcu
                                nisl, eget fermentum mauris.
                            </p>
                            <div class="info">
                                <div class="date">February 12, 2014,</div>
                                <div class="comments">2 comments</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="content m3" style="display: none">
                    <div class="item cf">
                        <div class="col-3">
                            <img src="http://placehold.it/77x77" alt=""/>
                        </div>
                        <div class="col-9">
                            <a class="title" href="single.html">A Standard Post Format</a>
                            <p>
                                Mauris ac gravida tellus. Etiam non arcu
                                nisl, eget fermentum mauris.
                            </p>
                            <div class="info">
                                <div class="date">February 12, 2014,</div>
                                <div class="comments">2 comments</div>
                            </div>
                        </div>
                    </div>
                    <div class="item cf">
                        <div class="col-3">
                            <img src="http://placehold.it/77x77" alt=""/>
                        </div>
                        <div class="col-9">
                            <a href="single.html" class="title">Our Children Matter</a>
                            <p>
                                Mauris ac gravida tellus. Etiam non arcu
                                nisl, eget fermentum mauris.
                            </p>
                            <div class="info">
                                <div class="date">February 12, 2014,</div>
                                <div class="comments">2 comments</div>
                            </div>
                        </div>
                    </div>
                    <div class="item cf">
                        <div class="col-3">
                            <img src="http://placehold.it/77x77" alt=""/>
                        </div>
                        <div class="col-9">
                            <a href="single.html" class="title">Care for Children</a>
                            <p>
                                Mauris ac gravida tellus. Etiam non arcu
                                nisl, eget fermentum mauris.
                            </p>
                            <div class="info">
                                <div class="date">February 12, 2014,</div>
                                <div class="comments">2 comments</div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <!-- Recent Events -->
            <div class="recent-events">

                <h3>Recent events</h3>

                <div class="item cf">
                    <div class="col-3">
                        <img src="http://placehold.it/77x77" alt=""/>
                    </div>
                    <div class="col-9">
                        <a class="title" href="single.html">Charity Marathon 2014</a>
                        <div class="info">
                            <div class="calendar">1 June, 2014 at 9:00 am - 21:00 pm</div>
                        </div>
                    </div>
                </div>

                <div class="item cf">
                    <div class="col-3">
                        <img src="http://placehold.it/77x77" alt=""/>
                    </div>
                    <div class="col-9">
                        <a class="title" href="single.html">Stop Children Hunger</a>
                        <div class="info">
                            <div class="calendar">1 June, 2014 at 9:00 am - 21:00 pm</div>
                        </div>
                    </div>
                </div>

            </div>

            <!-- Featured Video -->
            <div class="video">
                <h3>Featured video</h3>
                <iframe src="//www.youtube.com/embed/1kl8c_Dx1rY" allowfullscreen></iframe>
            </div>

        </aside>

    </div>
 @endsection