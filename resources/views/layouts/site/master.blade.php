<!DOCTYPE html>
<html>
<head>
	<!-- <title>App Name - @yield('title')</title> -->
	<meta charset="utf-8">
    <title>Donation</title>
    <meta name="description" content="Donation">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Styles -->
    <link rel="stylesheet" href="{{asset('css/general.css')}}">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <!-- Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>
</head>

<body>
<!-- Header Top -->
    <header class="top cf">
        <div class="center">
            <div class="tel">8 800 <span>524 90 26</span></div>

            <div class="right">
                <div class="block email">
                    <a href="#">Get news by E-mail</a>
                </div>
                <div class="block donate">
                    <a href="#">Donate</a>
                </div>
                <div class="block name">
                    Jon Millers
                </div>
                <div class="block search">
                    <form action="#" method="post">
                        <input type="search"/>
                        <input type="submit" value=""/>
                    </form>

                </div>
            </div>

        </div>
    </header>
    <!-- Header Main -->
    <header class="main">
        <div class="center">
            <div class="logo"><a href="index.html"></a></div>

            <!-- Navigation -->
            <nav>
                <ul>
                    <li class="active"><a href="{{URL::to('gallery/home')}}">Home</a></li>
                    <li>
                        <a href="about.html">Our causes</a>
                        <ul>
                            <li><a href="">Drop down example 1</a></li>
                            <li><a href="">Drop down example 2</a></li>
                            <li><a href="">Drop down example 3</a></li>
                            <li><a href="">Drop down example 4</a></li>
                            <li><a href="">Drop down example 5</a></li>
                            <li><a href="">Drop down example 6</a></li>
                        </ul>
                    </li>
                    <li><a href="events.html">Events</a></li>
                    <li><a href="blog.html">Blog</a></li>
                    <li><a href="donate.html">Donate</a></li>
                    <li><a href="{{URL::to('gallery')}}">Gallery</a></li>
                    <li><a href="contacts.html">Contacts</a></li>
                </ul>
            </nav>

            <div id="mobile-button"></div>
            <div id="mobile-menu">
                <ul>
                    <li class="active"><a href="index.html">Home</a></li>
                    <li>
                        <a href="#">Our causes</a>
                        <ul>
                            <li><a href="">Drop down example 1</a></li>
                            <li><a href="">Drop down example 1</a></li>
                            <li><a href="">Drop down example 1</a></li>
                            <li><a href="">Drop down example 1</a></li>
                            <li><a href="">Drop down example 1</a></li>
                            <li><a href="">Drop down example 1</a></li>
                            <li><a href="">Drop down example 1</a></li>
                        </ul>
                    </li>
                    <li><a href="events.html">Events</a></li>
                    <li><a href="blog.html">Blog</a></li>
                    <li><a href="donate.html">Donate</a></li>
                    <li><a href="gallery.html">Gallery</a></li>
                    <li><a href="contacts.html">Contacts</a></li>
                </ul>
            </div>
        </div>
    </header>
     <div id="slider"> 
      @section('sidebar')
      @show
    </div>
        <section>
           @yield('content')
         
        </section>
           
    <!-- Section Bottom -->
    <footer>
        <div class="line">
            <div class="center cf">
                <div class="links">
                    <a href="index.html">Home</a>
                    <a href="about.html">Our causes</a>
                    <a href="events.html">Events</a>
                    <a href="blog.html">Blog</a>
                    <a href="donate.html">Donate</a>
                    <a href="gallery.html">Gallery</a>
                    <a href="contacts.html">Contacts</a>
                </div>
                <div class="copy">© 2014  All rights reserved. <span>Donación</span> </div>
            </div>
        </div>

        <div class="footer">
            <div class="center cf">
                <div class="col-4">
                    <h3>Company information</h3>
                    <p>
                        It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.
                        The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here.
                    </p>
                    <div class="social">
                        <a href="#" class="twitter"></a>
                        <a href="#" class="facebook"></a>
                        <a href="#" class="google"></a>
                        <a href="#" class="flickr"></a>
                        <a href="#" class="youtube"></a>
                    </div>
                </div>
                <div class="col-4 twits">
                    <h3>Latest twits</h3>
                    <div class="twit">
                        <p>
                            It is a long established fact that a reader will be distracted by
                            the readable content of a page when looking at its layout.
                        </p>
                        <div class="name">@Donacion <span>2 hours ago</span></div>
                    </div>
                    <!-- -->
                    <div class="twit">
                        <p>
                            It is a long established fact that a reader will be distracted by
                            the readable content of a page when looking at its layout.
                        </p>
                        <div class="name">@Donacion <span>2 hours ago</span></div>
                    </div>
                </div>
                <div class="col-4 contacts">
                    <h3>Contacts</h3>
                    <div class="phone">8 800 <span>524 90 26</span></div>
                    <div class="adress">
                        <span>United States</span>
                        New York, Wall st. 25/1
                    </div>
                    <div class="email">info@donacion.cm</div>

                </div>
            </div>
        </div>
    </footer>

<!-- Libs -->
<!-- Lightbox -->
<script src="{{asset('lightbox2/dist/js/lightbox.js')}}"></script>
<script  src="{{asset('lightbox2/dist/js/lightbox-plus-jquery.js')}}"></script>
<link href="{{asset('lightbox2/dist/css/lightbox.css')}}" rel="stylesheet">
<!-- End Lightbox -->
    <script src="{{asset('js/libs/jquery-1.10.2.min.js')}}"></script>
    <script src="{{asset('js/libs/jquery.flexslider-min.js')}}"></script>
    <!-- Custom -->
    <script src="{{asset('js/scripts.js')}}"></script>

    <script>
        InitHome();
    </script>
</body>
</html>