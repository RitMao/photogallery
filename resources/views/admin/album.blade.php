
@extends('layouts.admin.adminmaster');
  @section('content')
 <span class="btn btn-primary"><a href="{{URL::to('page/create')}}" style="color: white;">New</a></span>
 <table class="table table-bordered">
  <thead>
    <tr>
      <th>#</th>
      <th class="text_center">Photo name</th>
      <th class="text_center">Status</th>
      <th class="text_center">action</th>
    </tr>
  </thead>
  <tbody>
  <?php $i=1;?>
    @foreach  ($record as $album)
      <tr>
      <th scope="row" class="text_center"><?php echo $i;?></th>
      <td class="text_center">{{$album->albumname}}</td>
      <td class="text_center">
                    @if($album->status == '1')
                    {!!html_entity_decode( Html::linkRoute("statusalbum", '<i class="fa fa-unlock-alt" aria-hidden="true"></i>',array( $album->status,$album->ID)) )!!}
                    @else
                    {!!html_entity_decode( Html::linkRoute("statusalbum", '<i class="fa fa-lock" aria-hidden="true"></i>', array( $album->status,$album->ID)))!!}
                    @endif
                </td>
      <td class="text_center">
                    {!!html_entity_decode( Html::linkRoute("delete",'<i class="fa fa-trash-o" aria-hidden="true"></i>', $album->ID,array('onclick' => 'return confirmDelete();')) ) !!}
                    |
                    {!!html_entity_decode( Html::linkRoute("edit", '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', $album->ID ) ) !!}
                </td>
          </tr>
           <?php $i++; ?>
      @endforeach 
  </tbody>
</table>
 @endsection