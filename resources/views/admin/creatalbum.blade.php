@extends('layouts.admin.adminmaster');
@section('content')
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-body">     
                {!! Form::open(array('url' => URL::to($URL), 'class' => 'cmxform form-horizontal tasi-form', 'method' => 'POST')) !!}   
               <div class="form-group">

                    <div class="form-group">
                        <div class="col-lg-1"> 
                            <input type="hidden" class="form-control" id="a1" name="suerID" value="{{Auth::user()->ID}}">
                    <input type="hidden" class="form-control" id="a1" name="ID" value="{!!$albums->ID!!}"> 
                        </div>  
                    </div> 
                    <div class="form-group">
                        <div class="col-lg-4"> 
                            <input type="text" class="form-control" id="a1" name="albumname" value="{!!$albums->albumname!!}"> 
                        </div>  
                    </div> 
                    <div class="form-group">
                        <div class="col-lg-offset-2 col-lg-2">
                            <button class="btn btn-success waves-effect waves-light" type="submit"><i class="fa fa-save"></i> Save</button>
                            <a href="{{URL::to('page/abums')}}" class="btn btn-default waves-effect">Cancel</a>
                        </div>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>


@stop