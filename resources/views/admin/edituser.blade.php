@extends('layouts.admin.adminmaster')

@section('PageTitle')
@endsection
@section('content')

<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">

            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">
                         
                          {!! Form::open(['route'=>['updatuser',$user->id],'method'=>'POST', 'files'=>true ,'class'=>'form-horizontal','name'=>'form'])!!}
                            <div class="form-group">
                                <label>Firstname</label>
                                <input class="form-control" name="firstname" value="{!!$user->firsname!!}">
                            </div>
                            <div class="form-group">
                                <label>Lastname</label>
                                <input class="form-control" name="lastname" value="{!!$user->lastname!!}">
                            </div>
                         </div>
                    <div class="col-lg-6">
                            <div class="form-group">
                                <label>Email</label>
                                <input class="form-control" name="email" value="{!!$user->email!!}">
                            </div>
                           {!! Form::submit('Save', array('class' => 'btn btn-info')) !!}  
                    </div>
                    
                    
                     {!! Form::close() !!}
                </div>
                <!-- /.col-lg-6 (nested) -->
            </div>
            <!-- /.row (nested) -->
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@endsection