@extends('layouts.admin.adminmaster')

@section('PageTitle')
{{ $PageTitle }}
@endsection

@section('content')

<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">

            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">
                           {!! Form::open(array('url' => URL::to($URL), 'class' => 'cmxform form-horizontal tasi-form', 'files'=>true,'method' => 'POST')) !!} 
                         {!! Form::hidden('ID',$photo->ID) !!}
                            <div class="form-group">
                                <label>Photo name</label>
                                <input class="form-control" name="photoname" value="{!!$photo->photoname!!}">
                            </div>
                            <div class="form-group">
                                <label>Album name</label>
                                <select style="width:500px; height:30px;" class="form-control" name="albumid">
                                    @foreach($album as $data)
                                    <option  value="{{$data->ID}}">{{$data->albumname}}</option>
                                    @endforeach
                                </select>
                      
                            </div>
                    </div>

                    <div class="col-lg-6">
                        <form role="form">
                            <div class="form-group">
                                <label>Photo description</label>
                                <input class="form-control" name="desc" value="{!!$photo->desc!!}">
                            </div>
                            <div class="form-group">
                                <label>Upload image</label>
                                <input class="form-control"  name="image" type="file" value="{!!$photo->image!!}">
                            </div>
                    </div>
                    {!! Form::submit('Save', array('class' => 'btn btn-info')) !!} 
                     {!! Form::close() !!}
                </div>
                <!-- /.col-lg-6 (nested) -->
            </div>
            <!-- /.row (nested) -->
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@endsection