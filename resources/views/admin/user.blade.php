
@extends('layouts.admin.adminmaster');
  @section('content')
 <span class="btn btn-primary"><a href="{{URL::to('page/createuser')}}" style="color: white;">New</a></span>
 <table class="table table-bordered">
  <thead>
    <tr>
      <th>#</th>
      <th class="text_center">User name</th>
      <th class="text_center">Email</th>
      <th class="text_center">Status</th>
      <th class="text_center">Action</th>
    </tr>
  </thead>
  <tbody>
   <?php $i = 1; ?>
    @foreach ($user as $users)
      <tr>
      <th scope="row" class="text_center"><?php echo $i; ?></th>
      <td class="text_center">{{$users->firsname}}{{$users->lastname }}</td>
      <td class="text_center">{{$users->email}}</td>
      <td class="text_center">
                    @if($users->status == '1')
                    {!!html_entity_decode( Html::linkRoute("userstatus", '<i class="fa fa-unlock-alt" aria-hidden="true"></i>',array( $users->status,$users->id)) )!!}
                    @else
                    {!!html_entity_decode( Html::linkRoute("userstatus", '<i class="fa fa-lock" aria-hidden="true"></i>', array( $users->status,$users->id)))!!}
                    @endif
                </td>
      <td class="text_center">
                    {!!html_entity_decode( Html::linkRoute("delteuser",'<i class="fa fa-trash-o" aria-hidden="true"></i>', $users->id,array('onclick' => 'return confirmDelete();')) ) !!}
                    |
                    {!!html_entity_decode( Html::linkRoute("edituser", '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', $users->id) ) !!}
                </td>
          </tr>
           <?php $i++; ?>
      @endforeach 
  </tbody>
</table>
 @endsection