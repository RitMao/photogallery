@extends('layouts.admin.adminmaster')

@section('PageTitle')
@endsection
@section('content')

<!-- /.row -->
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">

            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-6">
                          {!! Form::open(array('url'=>'page/saveuser','method' => 'POST')) !!}
                            <div class="form-group">
                                <label>Firstname</label>
                                <input class="form-control" name="firstname" value="">
                            </div>
                            <div class="form-group">
                                <label>Lastbane</label>
                                <input class="form-control" name="lastname" value="">
                            </div>
                         </div>
                    <div class="col-lg-6">
                            <div class="form-group">
                                <label>Email</label>
                                <input class="form-control" name="email" value="">
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                 <input class="form-control" placeholder="Password" name="password" type="password" value="">
                            </div>
                    </div>
                    <div class="col-lg-6">
                         <div class="form-group">
                                    <input class="form-control" placeholder="Confirm password" name="cornfirmpassword" type="password" value="">
                                    @if(Session::has('message'))
                                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                                    @endif
                        </div>
                    </div>
                    {!! Form::submit('Save', array('class' => 'btn btn-info')) !!} 
                     {!! Form::close() !!}
                </div>
                <!-- /.col-lg-6 (nested) -->
            </div>
            <!-- /.row (nested) -->
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
</div>
<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@endsection