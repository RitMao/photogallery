
@extends('layouts.admin.adminmaster');
  @section('content')
    <a class="btn icon-btn btn-success" href="{{URL::to('page/addphoto')}}">
<span class="glyphicon btn-glyphicon glyphicon-plus img-circle text-success"></span>
Add
</a>
<table class="table table-bordered">
  <thead>
    <tr>
      <th>#</th>
      <th class="text_center">Photo name</th>
      <th class="text_center">Photogallery</th>
      <th class="text_center">Status</th>
      <th class="text_center">action</th>
    </tr>
  </thead>
  <tbody>
  
    @foreach  ($Records as $photo)

      <tr>
      <th scope="row" class="text_center">{{$photo->ID}}</th>
      <td class="text_center">{{$photo->photoname}}</td>
      <td class="text_center"><img src='{{asset("img/photos/$photo->image")}}' alt="" style="width:120px; height:50px; " /></td>
       <td class="text_center">
                    @if($photo->status == '1')
                    {!!html_entity_decode( Html::linkRoute("phototatus", '<i class="fa fa-unlock-alt" aria-hidden="true"></i>',array( $photo->status,$photo->ID)) )!!}
                    @else
                    {!!html_entity_decode( Html::linkRoute("phototatus", '<i class="fa fa-lock" aria-hidden="true"></i>', array( $photo->status,$photo->ID)))!!}
                    @endif
                </td>
      <td class="text_center">
                    {!!html_entity_decode( Html::linkRoute("delte",'<i class="fa fa-trash-o" aria-hidden="true"></i>', $photo->ID,array('onclick' => 'return confirmDelete();'))) !!}
                    |
                    {!!html_entity_decode( Html::linkRoute("editphoto",'<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', $photo->ID)) !!}
                </td>
          </tr>
      @endforeach 

  </tbody>
</table>
 @endsection